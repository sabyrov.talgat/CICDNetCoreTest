#!/bin/bash
paths=(`find . -path '*Test.csproj'`)

for filePath in "${paths[@]}"
do
	result=$(eval 'dotnet test $filePath')	
	echo $result
done

